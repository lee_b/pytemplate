dist:
	poetry build

test:
	pytest --isort --flake8 --mypy --black --cov components/
	pylama components/

lint:
	pylint components/

format:
	black components/

